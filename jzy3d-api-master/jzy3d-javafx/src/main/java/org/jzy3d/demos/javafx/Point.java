package org.jzy3d.demos.javafx;

public class Point {
    double x;
    double y;
    double z;
    double funcNum;

    //generates Point with random values within set constraints
    public Point(double upperX, double lowerX, double upperY, double lowerY, double funcNum) {
        this.funcNum = funcNum;
    	this.x = (double) (Math.random() * (upperX - lowerX)) + lowerX;
        this.y = (double) (Math.random() * (upperY - lowerY)) + lowerY;
        this.z = this.genZ(funcNum);
    }

    //generates Point with set values
    public Point(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    //empty constructor
    public Point(){ }

    //returns x value
    public double getX(){
        return this.x;
    }

    //returns y value
    public double getY(){
        return this.y;
    }

    //returns z value
    public double getZ(){
        return this.z;
    }

    /**
     * set Function and generate z from it
     * @author Ennin Samuel
     * @param funcNum
     * @return double z
     */
    private double genZ(double funcNum) {
    	double z = 1.0;
    	switch((int)funcNum) {
    	//Function 1
    	case 1:
    		z = (double) 1- Math.pow(x, 2) - Math.pow(y, 2);
    		break;
		//Function 2
    	case 2:
    		z = (double) -1 + Math.pow( (this.x - 0.5), 2) +  Math.pow( (this.y + 0.2), 2);
    		break;
		//Function 3
    	case 3:
    		z = (double) 1- Math.pow(x, 2) - Math.pow(y, 2) + Math.sin(Math.pow(this.x, 2));
    		break;
		//Function 4
    	case 4:
    		z = (double) 1- Math.pow(x, 2) - Math.pow(y, 2) + Math.sin(5 * this.y + Math.pow(this.x, 2));
    		break;
		//Function 5
    	case 5:
    		z = (double) 1 / 2 * x * Math.sin(2 * Math.pow(y, 2) + 2 * Math.pow(x,2));
    		break;
    		
    	}
    	return z;
    }

	@Override
	public String toString() {
		return "Point [x=" + x + ", y=" + y + ", z=" + z + ", funcNum=" + funcNum + "]";
	}
    
}
