package org.jzy3d.demos.javafx;

import java.util.ArrayList;

public class SimulatedAnnealing {

    //define initial point array
    private ArrayList<Point> initialPoints = new ArrayList<>();
    //set starting temperature
    private double temp = 1.0;
    //set cooling rate
    private double coolingRate = 0.0001;
    //set constraints
    double upperXConstraint = 10.0;
    double lowerXConstraint = -10.0;
    double upperYConstraint = 10.0;
    double lowerYConstraint = -10.0;
    double funcNum;
    
    /**
     * Return results of the calculations
     * @return result
     */
    public String genResults() {
        this.iteratePoints();
        ArrayList<Point> printList = this.annealing(this.initialPoints);
        String results = "Final Lowest:\n" + printList.get(0).toString() + "\n\nFinal Highest:\n" + printList.get(1).toString(); 
		return results;
    }
    public void initialize(double bereich, double funcNum) {
    	this.funcNum = funcNum;
    	upperXConstraint = bereich;
        lowerXConstraint = -bereich;
        upperYConstraint = bereich;
        lowerYConstraint = -bereich;
    }


    public static void main(String[] args) {
        SimulatedAnnealing sa = new SimulatedAnnealing();
        sa.iteratePoints();
        ArrayList<Point> printList = sa.annealing(sa.initialPoints);
        System.out.println("--------------------------");
        System.out.println("Final Lowest:");
        sa.printPoint(printList.get(0));
        System.out.println("Final Highest");
        sa.printPoint(printList.get(1));
        System.out.println("--------------------------");
    }

    //this is where the simulated annealing happens
    public ArrayList<Point> annealing(ArrayList<Point> initial){
        ArrayList<Point> highPoints = new ArrayList<>();
        initial = quickSort(initial);
        Point highest = initial.get(initial.size()-1);
        Point lowest = initial.get(0);
        for(int i = 10; i >= 1; i--){
            highPoints.add(initial.get(initial.size()-i));
        }
        while(temp > 0.1){
            highPoints = quickSort(highPoints);
            ArrayList<Point> tempPoints = new ArrayList<>();
            for(int i = highPoints.size()-10; i < highPoints.size(); i++){
                Point p = highPoints.get(i);
                double x = p.getX();
                double y = p.getY();
                for(int z = 0; z < 10; z++){
                    Point newPoint = new Point(checkCon(upperXConstraint, x + temp, true),
                                            checkCon(lowerXConstraint, x - temp, false),
                                            checkCon(upperYConstraint, y + temp, true),
                                            checkCon(lowerYConstraint, y - temp, false), funcNum);
                    if(newPoint.getZ() < lowest.getZ()){
                        lowest = newPoint;
                    }
                    if(newPoint.getZ() >= highPoints.get(highPoints.size()-10).getZ()){
                        tempPoints.add(newPoint);
                        if(newPoint.getZ() > highest.getZ()){
                            highest = newPoint;
                        }
                    }
                }
            }
            highPoints.addAll(tempPoints);
            temp *= 1 - coolingRate;
            System.out.println("Current highest: ( " + highest.getX() + " | " + highest.getY() + " | " + highest.getZ() + " )");
            System.out.println("temperature: " + temp);
        }
        ArrayList<Point> finalPoints = new ArrayList<>();
        finalPoints.add(lowest);
        finalPoints.add(highest);
        return (finalPoints);
    }

    //checks if generated coordinate is within set constraints
    public double checkCon(double constraint, double tempCoord, boolean isUpper){
        if(tempCoord > constraint && isUpper){
            tempCoord = constraint;
        }else if(tempCoord < constraint && !isUpper){
            tempCoord = constraint;
        }
        return tempCoord;
    }

    //iterate a basic list of points
    public void iteratePoints(){
        int pointNum = 100;
        for(int i = 0; i < pointNum; i++){
            Point point = new Point(upperXConstraint, lowerXConstraint, upperYConstraint, lowerYConstraint, funcNum);
            initialPoints.add(point);
        }
    }

    //print out a list of points
    public void printPoints(ArrayList<Point> arrayList){
        for(Point i : arrayList){
            System.out.println("( " + i.getX() + " | " + i.getY() + " | " + i.getZ() + " )");
            //System.out.println(temp);
        }
    }

    //print out a single point
    public void printPoint(Point p){
        System.out.println("( " + p.getX() + " | " + p.getY() + " | " + p.getZ() + " )");
    }

    //Sorting algorithm
    public ArrayList<Point> quickSort(ArrayList<Point> array) {
        if(array.size() <= 1){
            return array;
        }
        ArrayList<Point> sortedPoints = new ArrayList<>();
        ArrayList<Point> smallerPoints = new ArrayList<>();
        ArrayList<Point> biggerPoints = new ArrayList<>();
        Point marker = array.get(array.size()-1);
        for ( int i = 0; i < array.size()-1; ++i ) {
            if (array.get(i).getZ() < marker.getZ()) {
                smallerPoints.add(array.get(i));
            } else {
                biggerPoints.add(array.get(i));
            }
        }
        smallerPoints = quickSort(smallerPoints);
        biggerPoints = quickSort(biggerPoints);

        smallerPoints.add(marker);
        smallerPoints.addAll(biggerPoints);
        sortedPoints = smallerPoints;

        return sortedPoints;
    }

	public ArrayList<Point> getInitialPoints() {
		return initialPoints;
	}

	public void setInitialPoints(ArrayList<Point> initialPoints) {
		this.initialPoints = initialPoints;
	}
    

}
