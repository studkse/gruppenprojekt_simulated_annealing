package org.jzy3d.demos.javafx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.jzy3d.chart.AWTChart;
import org.jzy3d.colors.Color;
import org.jzy3d.colors.ColorMapper;
import org.jzy3d.colors.colormaps.ColorMapRainbow;
import org.jzy3d.javafx.JavaFXChartFactory;
import org.jzy3d.javafx.JavaFXRenderer3d;
import org.jzy3d.javafx.controllers.mouse.JavaFXCameraMouseController;
import org.jzy3d.maths.Range;
import org.jzy3d.plot3d.builder.Builder;
import org.jzy3d.plot3d.builder.Mapper;
import org.jzy3d.plot3d.primitives.Shape;
import org.jzy3d.plot3d.rendering.canvas.Quality;

/**
 * Showing how to pipe an offscreen Jzy3d chart image to a JavaFX ImageView.
 * 
 * {@link JavaFXChartFactory} delivers dedicated  {@link JavaFXCameraMouseController}
 * and {@link JavaFXRenderer3d}
 * 
 * Support 
 * Rotation control with left mouse button hold+drag
 * Scaling scene using mouse wheel 
 * Animation (camera rotation with thread) 
 * 
 * TODO : 
 * Mouse right click shift
 * Keyboard support (rotate/shift, etc)
 * 
 * @author Martin Pernollet
 * 
 */

/**
 * Separated Methods, which were added: makeRange(), makeFuncNum(),
 * getBereich(), getFuncNum()
 */
public class MainApplication extends Application {
	double bereich, funcNum;

	public static void main(String[] args) {
		Application.launch(args);
	}

	// Set Range
	private void makeRange() {
		boolean ok = true;
		do {
			// Validate Input
			try {
				String suchbereich = JOptionPane
						.showInputDialog("Geben Sie den Suchbereich ein\nBeispiel: 10.0 oder 5.0");
				double bereich = Double.parseDouble(suchbereich);
				this.bereich = bereich;
				ok = true;
			} catch (NumberFormatException e) {
				ok = false;
				JOptionPane.showMessageDialog(null,
						"Geben Sie eine Zahl im folgenden Format ein: 'Number.Number'.\nBeispiel: 3.0 ");
			}
		} while (!ok);
	}

	// Set Function
	private void makeFuncNum() {
		boolean ok = true;
		do {
			// Validate Input
			try {
				String funcNumString = JOptionPane.showInputDialog("Geben Sie die Funktionsnummer ein:\n"
						+ "[1] f_1 (x,y) = 1 - x� - y�\n" + "[2] f_2 (x,y) = -1 + (x - 0.5)� + (y + 0.2)�\n"
						+ "[3] f_3 (x,y) = 1 - x� - y� + sin(x�)\n"
						+ "[4] f_4 (x,y) = 1 - x� - y� + sin(5y + x�)\n" + "[5] f_5 (x,y) = 1 / 2 x sin(2y� + 2x�)"
						+ "\nBeispiel: 1.0 oder 2.0");

				funcNum = Double.parseDouble(funcNumString);
				ok = true;
			} catch (NumberFormatException e) {
				ok = false;
				JOptionPane.showMessageDialog(null,
						"Geben Sie die Funktionsnummer im folgenden Format ein: 'Number.Number'.\nBeispiel: 1.0 ");
			}
		} while (!ok);

	}

	// Getters
	public double getBereich() {
		return bereich;
	}

	public double getFuncNum() {
		return funcNum;
	}

	@Override
	public void start(Stage stage) {
		makeRange();
		makeFuncNum();
		SimulatedAnnealing sa = new SimulatedAnnealing();
		sa.initialize(getBereich(), getFuncNum());

		JOptionPane.showMessageDialog(null, sa.genResults() + "\n\n => Exit this Pane to see the 3D model");

		stage.setTitle("DemoJzy3dFX");

		// Jzy3d
		JavaFXChartFactory factory = new JavaFXChartFactory();
		AWTChart chart = getDemoChart(factory, "offscreen");
		ImageView imageView = factory.bindImageView(chart);

		// JavaFX
		StackPane pane = new StackPane();
		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
		pane.getChildren().add(imageView);

		factory.addSceneSizeChangedListener(chart, scene);

		stage.setWidth(500);
		stage.setHeight(500);

	}

	private AWTChart getDemoChart(JavaFXChartFactory factory, String toolkit) {
		// -------------------------------
		// Define a function to plot
		Mapper mapper = new Mapper() {
			@Override
			public double f(double x, double y) {

				double z = 1.0;
				switch ((int) funcNum) {
				// Function 1
				case 1:
					z = (double) 1 - Math.pow(x, 2) - Math.pow(y, 2);
					break;
				// Function 2
				case 2:
					z = (double) -1 + Math.pow((x - 0.5), 2) + Math.pow((y + 0.2), 2);
					break;
				// Function 3
				case 3:
					z = (double) 1 - Math.pow(x, 2) - Math.pow(y, 2) + Math.sin(Math.pow(x, 2));
					break;
				// Function 4
				case 4:
					z = (double) 1 - Math.pow(x, 2) - Math.pow(y, 2) + Math.sin(5 * y + Math.pow(x, 2));
					break;
				// Function 5
				case 5:
					z = (double) 1 / 2 * x * Math.sin(2 * Math.pow(y, 2) + 2 * Math.pow(x, 2));
					break;
				default:
					z = (double) 1 - Math.pow(x, 2) - Math.pow(y, 2);
					break;

				}
				return z;
			}
		};

		// Define range and precision for the function to plot
		Range range = new Range(-(int) bereich, (int) bereich);
		int steps = 80;

		// Create the object to represent the function over the given range.
		final Shape surface = Builder.buildOrthonormal(mapper, range, steps);
		surface.setColorMapper(new ColorMapper(new ColorMapRainbow(), surface.getBounds().getZmin(),
				surface.getBounds().getZmax(), new Color(1, 1, 1, .5f)));
		surface.setFaceDisplayed(true);
		surface.setWireframeDisplayed(false);

		// -------------------------------
		// Create a chart
		Quality quality = Quality.Advanced;
		quality.setSmoothPolygon(true);
		quality.setAnimated(true);

		// let factory bind mouse and keyboard controllers to JavaFX node
		AWTChart chart = (AWTChart) factory.newChart(quality, toolkit);
		chart.getScene().getGraph().add(surface);
		return chart;
	}
}
